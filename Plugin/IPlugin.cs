﻿using System.Collections.Generic;

namespace Plugin
{
    /// <summary>
    /// Simple interface that defines the structure of a plugin.
    /// </summary>
    public interface IPlugin
    {
        /// <summary>
        /// Placeholder for the Execute method.
        /// </summary>
        /// <param name="_command"></param>
        void Execute(string _command);
        /// <summary>
        /// Placeholder for the list of commands of the plugin.
        /// </summary>
        IList<string> Commands { get; set; }
    }
}