﻿using Plugin;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace TestPlugin
{
    /// <summary>
    /// A simple implementation of a plugin.
    /// </summary>
    public class TestPluginImplementation : IPlugin
    {
        /// <summary>
        /// Private property that holds the commands.
        /// </summary>
        private List<string> _commands;

        /// <summary>
        /// Implementation of the command list.
        /// </summary>
        public IList<string> Commands
        {
            get
            {
                return _commands;
            }

            set
            {
                _commands.AddRange(value);
            }
        }

        /// <summary>
        /// Constructor. Initializes the commands of the plugin.
        /// </summary>
        public TestPluginImplementation()
        {
            _commands = new List<string>();
            _commands.Add("SAY_HELLO");
        }

        /// <summary>
        /// The distributing method. Covers all cases of commands and forward them.
        /// </summary>
        /// <param name="_command">The command to execute.</param>
        public void Execute(string _command)
        {
            switch (_command.ToUpper())
            {
                case "SAY_HELLO":
                    SayHello();
                    break;
                default:
                    ShowErrorMessage();
                    break;
            }
        }

        /// <summary>
        /// Quite simple method to show in which assembly we are.
        /// </summary>
        private void SayHello()
        {
            Console.WriteLine("Hello World! I'm from " + Assembly.GetExecutingAssembly().FullName);
        }

        /// <summary>
        /// Also very simple method to show an error message. This method shouldn't be executed.
        /// </summary>
        private void ShowErrorMessage()
        {
            Console.WriteLine("Wrong command for " + Assembly.GetExecutingAssembly().FullName);
        }
    }
}
