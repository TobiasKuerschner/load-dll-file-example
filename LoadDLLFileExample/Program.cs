﻿using System;
using System.IO;

namespace LoadDLLFileExample
{
    /// <summary>
    /// The main class that will be the entry point of the application.
    /// </summary>
    class Program
    {
        /// <summary>
        /// Entry point of the application.
        /// </summary>
        /// <param name="args">'args' are optional parameters that you can input via console when you start the application.</param>
        static void Main(string[] args)
        {
            // just to show where we are :D
            Console.WriteLine("Hello World! I'm from the main application!");

            // the directory path of the plugins
            var _pluginsPath = Directory.GetCurrentDirectory() + "\\plugins\\";

            // instanciate the helper class
            var _manager = new PluginManager(_pluginsPath);

            // set the command string
            var _command = "SAY_HELLO";

            // Execute the command string
            _manager.Execute(_command);
        }
    }
}