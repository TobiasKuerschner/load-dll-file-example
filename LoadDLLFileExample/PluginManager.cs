﻿using Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace LoadDLLFileExample
{
    class PluginManager
    {
        /// <summary>
        /// Dictionary that saves the information of the commands specified in the plugins. Key is the command string. Value is the method that then will be executed.
        /// </summary>
        public Dictionary<string, Action> Commands { get; set; } = new Dictionary<string, Action>();

        /// <summary>
        /// Manages the commands of all plugins that implement the 'IPlugin' interface in the chosen path.
        /// </summary>
        /// <param name="_pluginsPath">The path where the *.dll files are located.</param>
        public PluginManager(string _pluginsPath)
        {
            // check if the directory 'plugins' exists, if not create it
            if (!Directory.Exists(_pluginsPath))
                Directory.CreateDirectory(_pluginsPath);

            // files is a string[] with the complete paths of all files in the 'plugins' directory
            var files = Directory.GetFiles(_pluginsPath);

            // filter the files for files with extension '*.dll'
            var _dlls = files.Where(s => s.Contains(".dll")).ToList();

            // contains all types/classes that implement the 'IPlugin' interface
            var _types = new List<Type>();

            // iterate over all .dll-files
            foreach (var _dll in _dlls)
            {
                // get assembly of the .dll-file
                var _assembly = Assembly.LoadFile(_dll);

                // get all types of that assembly
                var _aTypes = _assembly.GetTypes();

                // add all types to the _types list that implements the 'IPlugin' interface and are not abstract and are not an interface, so we catch all classes that we can instantiate 
                foreach (var _aType in _aTypes)
                    // Thank you for the hint =)
                    if (typeof(IPlugin).IsAssignableFrom(_aType))
                        _types.Add(_aType);
            }

            // iterate over all plugin classes
            foreach (var _type in _types)
            {
                // create an instance of the plugin
                var instance = Activator.CreateInstance(_type);

                // cast it to 'IPlugin' to handle the commands of the plugin
                var plugin = ((IPlugin)instance);

                // add the commands with suitable execute method to the command dictionary
                foreach (var _com in plugin.Commands)
                    Commands.Add(_com, () => plugin.Execute(_com));
            }
        }

        /// <summary>
        /// Method to execute a command.
        /// </summary>
        /// <param name="_command">The command to execute as string.</param>
        public void Execute(string _command)
        {
            // try to get fitting method & execute it
            // Thank you for the hint =)
            if (Commands.TryGetValue(_command, out var _com))
                _com.Invoke();
        }
    }
}
