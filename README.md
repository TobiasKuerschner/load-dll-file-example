# README #

This project only shows how easy it is to write modular applications with .Net Framework 4.6.1 and C#. It only contains a simple console application, a plugin manager class, the definition of a plugin as an interface and an implementation of that interface.

### What is this repository for? ###

I use this repository to share this code on my blog at http://tobiaskuerschner.weebly.com/blog and I would be very happy if you will visit it. :D

### How do I get set up? ###

* Open the solution in Visual Studio 2017.
* Only -> build <- the project of the plugin implementation.
* Build and execute the LoadDLLFileExample project.
* See what happens... :D

### Contribution guidelines ###

* If you want to reuse the code please mention this repository or my webite. Thank you! :)